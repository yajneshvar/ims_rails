class AddAvailableCountToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :available_count, :integer
  end
end
