class RemoveBooksIdFromTransactions < ActiveRecord::Migration[5.0]
  def change
    remove_index :transactions, :books_id
  end
end
