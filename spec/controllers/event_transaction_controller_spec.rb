require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do
  let(:user) {FactoryGirl.create(:user)}
  let(:sale_transaction) {FactoryGirl.create(:transaction)}
  let(:confirmed_consignment_transaction) {FactoryGirl.create(:transaction, :consignment, :confirmed ,initial_count: 2, end_count: 1)}
  let(:event) {FactoryGirl.create(:event)}
  let(:event_with_transactions) {FactoryGirl.create(:event_with_transactions)}
  let(:book){FactoryGirl.create(:book)}
  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user).and_return(user)
  end


  context "Event transactions" do
    before(:each) do
      @transaction = confirmed_consignment_transaction
      @book = @transaction.book
      @initial_available_count = @book.available_count
      @initial_sold_count = @book.sold_count
      @beginning_initial_count = @transaction.initial_count
      @initial_end_count = @transaction.end_count
    end

    it "create transactions bound to event" do
      @event = event
      post :create, params: {event_id: @event.id ,transaction: confirmed_consignment_transaction.attributes()}
      @event.reload
      expect(response).to redirect_to(event_path(@event.id))
      expect(flash[:notice]).to eq "Transaction was successfully executed."
      expect(@event.transactions.size).to be 1
    end

    it "update transactions bound to event" do
      @event = event_with_transactions
      @transaction = @event.transactions.first
      @book = @transaction.book
      @initial_available_count = @book.available_count
      @initial_sold_count = @book.sold_count
      @beginning_initial_count = @transaction.initial_count
      @initial_end_count = @transaction.end_count
      updated_by = 2
      post :update, params: {id: @transaction.id,event_id: @event.id ,transaction: {initial_count: @beginning_initial_count + updated_by}}
      @event.reload
      @book.reload
      expect(response).to redirect_to(event_path(@event.id))
      expect(flash[:notice]).to eq "Transaction was successfully updated"
      expect(@book.available_count).to be @initial_available_count - updated_by
      expect(@book.sold_count).to be @initial_sold_count + updated_by
    end


  end



end
