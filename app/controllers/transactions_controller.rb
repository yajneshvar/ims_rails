class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :setup_trans_service, only: [:update]
  #before_action :set_event, :authenticate_user!
  before_action :authenticate_request!
  def create
      transaction_service = TransactionService.new(transaction_params, nil)
      status = transaction_service.create_transaction()
      @transactions = status[:transaction]
        if status[:success]
          render json: @transactions, status: :created, location: @transactions
        else
         render json: @transactions.errors, status: :unprocessable_entity
        end
  end

  def new
    @transactions = Transaction.new
  end

  def show
    render json: @transactions
  end

  def edit

  end

  def index
    @transactions = Transaction.all
    render json: @transactions, status: :ok
  end

  def update
    status = @transaction_service.update_transaction()
      if status[:success]
        render json: status[:transaction], status: :ok, location: @transactions
      else
        render json: @transactions.errors, status: :unprocessable_entity
      end


  end

  def destroy
    @transaction_service = TransactionService.new(nil,@transactions)
    status = @transaction_service.delete_transaction()
      if status[:success]
        render json: @transactions
      else
        render json: @transactions.errors, status: :unprocessable_entity
      end
  end


  private

  def transaction_params
    user_params = params.permit(:book_id,:quantity,:activity,:status,:origin_location,:final_location,:suggested_count,:initial_count,:end_count,:sale_price,:amount_paid)
    user_params = user_params.merge({issued_by: present_user.email})
    if(!event_id.nil?)
      user_params = user_params.merge({event_id: event_id})
    end
    user_params
  end

  def book_id
    params.permit(:book_id)
    params["book_id"]
  end

  def set_transaction
    @transactions = Transaction.find(params[:id])
  end

  def setup_trans_service
    @transaction_service = TransactionService.new(transaction_params,@transactions)
  end

  def set_event
    params.permit(:event_id)
    @event = params["event_id"].nil? ? nil: Event.find(params['event_id'])
  end

  def event_id
    params.permit(:event_id)
    Rails.logger.info("Event id is #{params["event_id"]}")
    params["event_id"]
  end
end


