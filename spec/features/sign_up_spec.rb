require "rails_helper"


RSpec.feature "User can sign_up" do
  scenario "When providing valid details" do
    visit "/"
    click_link "Sign Up"
    fill_in "Email", with: "yajneshvar.arumugam@gmail.com"
    fill_in "user_password", with: "password"
    fill_in "Password confirmation", with: "password"
    click_button "Sign up"
    expect(page).to have_current_path("/")
  end
end
