class AddDefaultToTransactions < ActiveRecord::Migration[5.0]
  def change
    change_column_default :transactions, :sale_price, from: nil, to: 0
    change_column_default :transactions, :amount_paid, from: nil, to: 0
    change_column_default :transactions, :pending_amount, from: nil, to: 0
  end
end
