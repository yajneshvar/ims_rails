import React from 'react';
import { Button,FormGroup,HelpBlock,FormControl,ControlLabel } from 'react-bootstrap';


class LogIn extends React.Component{
    constructor(props) {
        super(props);
        this.state = {email: "", password: ""};

        this.handleEmailChange = (e) => {
            this.setState({ email: e.target.value });
        };

       this.handleInputChange = (event) => {
            const target = event.target;
            const value = target.type === 'checkbox' ? target.checked : target.value;
            const name = target.name;

            this.setState({
                [name]: value
            });
        };

        this.handleSubmit = (event) => {
            alert('An essay was submitted: ' + this.state.email);
            event.preventDefault();
        };

    }


    render(){

        return (
            <form onSubmit={this.handleSubmit}>
                <FormGroup
                    controlId="formEmailAddress"
                >
                    <ControlLabel>Email Address</ControlLabel>
                    <FormControl
                        type="email"
                        name="email"
                        onChange = {this.handleInputChange}
                        value = {this.state.email}
                    />
                    <FormControl.Feedback />
                </FormGroup>

                <FormGroup
                    controlId="formPassword"
                >
                    <ControlLabel>Password</ControlLabel>
                    <FormControl
                        name="password"
                        type="password"
                        onChange = {this.handleInputChange}
                        value = {this.state.password}
                    />
                    <FormControl.Feedback />
                </FormGroup>

                <Button type="submit">
                    Log In
                </Button>
            </form>
        );

    }
}


export default LogIn;
