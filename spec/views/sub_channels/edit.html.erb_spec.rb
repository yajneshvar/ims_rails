require 'rails_helper'

RSpec.describe "sub_channels/edit", type: :view do
  before(:each) do
    @sub_channel = assign(:sub_channel, SubChannel.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit sub_channel form" do
    render

    assert_select "form[action=?][method=?]", sub_channel_path(@sub_channel), "post" do

      assert_select "input#sub_channel_name[name=?]", "sub_channel[name]"
    end
  end
end
