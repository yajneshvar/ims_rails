json.array!(@sub_channels) do |sub_channel|
  json.extract! sub_channel, :id, :name
  json.url sub_channel_url(sub_channel, format: :json)
end
