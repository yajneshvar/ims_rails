class RemoveBooksIdColumnFromTransactions < ActiveRecord::Migration[5.0]
  def change
    remove_column :transactions, :books_id, :integer
  end
end
