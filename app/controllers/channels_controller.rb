class ChannelsController < ApplicationController
  before_action :set_channel, only: [:show, :edit, :update, :destroy]

  # GET /channels
  # GET /channels.json
  def index
    @channels = Channel.all
    render json: @channels
  end

  # GET /channels/1
  # GET /channels/1.json
  def show
    render json:@channel
  end

  # GET /channels/new
  def new
    @channel = Channel.new
  end

  # GET /channels/1/edit
  def edit
  end

  # POST /channels
  # POST /channels.json
  def create
    @channel = Channel.new(channel_params)
      if @channel.save
        render json: @channel, status: :created, location: @channel
      else
        render json: @channel.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /channels/1
  # PATCH/PUT /channels/1.json
  def update
      if @channel.update(channel_params)
        render json: @channel, status: :ok, location: @channel
      else
        render json: @channel.errors, status: :unprocessable_entity
      end
  end

  # DELETE /channels/1
  # DELETE /channels/1.json
  def destroy
    @channel.destroy
    render json: @channel
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def channel_params
      params.permit(:name)
    end
end
