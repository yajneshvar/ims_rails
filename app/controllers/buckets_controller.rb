class BucketsController < ApplicationController
  before_action :set_bucket, only: [:show, :edit, :update, :destroy]

  # GET /buckets
  # GET /buckets.json
  def index
    @buckets = Bucket.all
    render json: @buckets
  end

  # GET /buckets/1
  # GET /buckets/1.json
  def show
    render json: @bucket
  end

  # GET /buckets/new
  def new
    @bucket = Bucket.new
  end

  # GET /buckets/1/edit
  def edit
  end

  # POST /buckets
  # POST /buckets.json
  def create
    @bucket = Bucket.new(bucket_params)
      if @bucket.save
        render json: @bucket, status: :created, location: @bucket
      else
        render json: @bucket.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /buckets/1
  # PATCH/PUT /buckets/1.json
  def update
      if @bucket.update(bucket_params)
        render json: @bucket, status: :ok, location: @bucket
      else
        render json: @bucket.errors, status: :unprocessable_entity
      end
  end

  # DELETE /buckets/1
  # DELETE /buckets/1.json
  def destroy
    @bucket.destroy
      render json: @bucket, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bucket
      @bucket = Bucket.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bucket_params
      params.permit(:name)
    end
end
