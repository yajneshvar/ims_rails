class SaleTransaction

  def initialize(trans,book)
    @transaction = trans
    @book = book
    @old_book = @transaction.book_id_changed? && !@transaction.book_id_was.nil? ? Book.find(@transaction.book_id_was) : nil
  end

  def create_transaction
    #creating a new sale
    status = {success: true}
    status[:transaction] = @transaction
    begin
    @transaction.transaction do
      if @transaction.staged?
        @book.update_available_count(@transaction.quantity)
      else
        @book.update_total_count(@transaction.quantity)
      end
     @transaction.save!
    end
    rescue ActiveRecord::Rollback,ActiveRecord::RecordInvalid => e
      Rails.logger.error "Record is invalid #{e.message}"
      status[:success] = false
      status[:msg] = @transaction.errors.messages
    end

    return status
  end

  def delete_transaction
    status = {success: true, transaction: @transaction}
    begin
      Transaction.transaction do
        @transaction.lock!
        if @transaction.staged?
          @book.update_available_count(@transaction.quantity,false)
        else
          @book.update_total_count(@transaction.quantity,false)
        end
        @transaction.destroy!
      end
    rescue  ActiveRecord::Rollback,ActiveRecord::RecordNotDestroyed => e
      Rails.logger.error "Record is not destroyed #{e.message}"
      status[:success] = false
      status[:msg] = e.message
    end
    return status
  end


  def undo_transaction
    status = {success: true}
    transaction_status = @transaction.status_changed? ? @transaction.status_was : @transaction.status
    quantity = @transaction.quantity_changed? ? @transaction.quantity_was : @transaction.quantity
    book = @old_book.nil? ? @book : @old_book
    begin
      Transaction.transaction do
        if transaction_status == "staged"
          book.update_available_count(quantity,false)
        else
          book.update_total_count(quantity,false)
        end
      end
    rescue  ActiveRecord::Rollback, ActiveRecord::RecordInvalid => e
      Rails.logger.error "Record is invalid #{e.message}"
      status[:success] = false
      status[:msg] = e.message
    end
    status[:transaction] = @transaction
    return status
  end

  def update_transaction
    # check important fields that have changed and do the necessary changes
    # fields that matter => quantity, book_id, status
    status = undo_transaction()
    if !status[:success]
      return status
    end
    create_transaction()
  end

private

  def update_quantity
    if @transaction.quantity_changed?
      book = @old_book.nil? ? @book : @old_book
      old_val = @transaction.quantity_was
      diff = (@transaction.quantity - old_val).abs
      reduce = diff > 0
      @transaction.status_was == "staged" ? book.update_available_count(diff,reduce) : book.update_total_count(diff,reduce)
      return true
    else
      return false
    end
  end

  def update_book
    return true if @old_book.nil?
    #undo the previous books transaction
    status = undo_transaction()
    return false unless status[:success]
    #redo this new transaction
    create_transaction()
  end

  def update_status
    return false unless @transaction.status_changed?
    old_status = @transaction.status_was
    new_status = @transaction.status
    if old_status == "staged" && new_status == "confirmed"
      @book.add_sold_count(@transaction.quantity)
    else
      @book.sub_sold_count(@transaction.quantity)
    end
    return true
  end
end