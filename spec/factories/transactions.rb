FactoryGirl.define do
  factory :transaction do
    quantity 2
    issued_by "yajneshvar.arumugam@gmail.com"
    sale_price ""
    amount_paid ""
    pending_amount ""
    origin_location "BookRoom1"
    trait :sale do
      activity :sale
    end
    trait :consignment do
      activity :consignment
      initial_count 5
      end_count 1
    end
    trait :restock do
      activity :restock
    end
    trait :issue do
      activity :issue
    end
    trait :score do
      activity :score
    end
    trait :confirmed do
      status :confirmed
    end
    trait :staged do
      status :staged
    end
    book
    event
  end
end
