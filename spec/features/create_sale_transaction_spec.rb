require "rails_helper"

RSpec.feature "User can create a sale transaction" do
  let!(:user) {FactoryGirl.create(:user)}
  let!(:transaction) {FactoryGirl.create(:transaction)}

  before do
    @book = FactoryGirl.create(:book)
    login_as(user)
  end

  scenario "Create new sale transaction" do
    initial_av_count = @book.available_count
    initial_sold_count = @book.sold_count
    visit new_transaction_path
    select @book.code, from: 'transaction_book_id'
    fill_in 'Quantity', with: 2
    select 'Sale', from: 'Activity'
    select 'Confirmed', from: 'Status'
    fill_in 'Origin location', with: 'BookRoom1'
    click_button 'Create Transaction'
    expect(page).to have_content('Transaction was successfully executed')
    @book.reload
    expect(@book.available_count).to be initial_av_count - 2
    expect(@book.sold_count).to be initial_sold_count + 2
  end


end
