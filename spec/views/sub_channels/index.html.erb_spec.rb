require 'rails_helper'

RSpec.describe "sub_channels/index", type: :view do
  before(:each) do
    assign(:sub_channels, [
      SubChannel.create!(
        :name => "Name"
      ),
      SubChannel.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of sub_channels" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
