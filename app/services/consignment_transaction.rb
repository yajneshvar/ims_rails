class ConsignmentTransaction

  def initialize(trans,book)
    @transaction = trans
    @book = book
    @old_book = @transaction.book_id_changed? && !@transaction.book_id_was.nil? ? Book.find(@transaction.book_id_was) : nil
  end

  def create_transaction
    #creating a new sale
    status = {success: true}
    status[:transaction] = @transaction
    quantity = get_current_quantity()
    @transaction.quantity = quantity
    begin
    @transaction.transaction do
      if @transaction.staged?
        @book.update_available_count(quantity)
      else
        @book.update_total_count(quantity)
      end
     @transaction.save!
    end
    rescue ActiveRecord::Rollback,ActiveRecord::RecordInvalid => e
      Rails.logger.error "Record is invalid #{e.message}"
      status[:success] = false
      status[:msg] = @transaction.errors.messages
    end
    return status
  end

  def delete_transaction
    status = {success: true, transaction: @transaction}
    begin
      Transaction.transaction do
        @transaction.lock!
        if @transaction.staged?
          @book.update_available_count(@transaction.quantity,false)
        else
          @book.update_total_count(@transaction.quantity,false)
        end
        @transaction.destroy!
      end
    rescue  ActiveRecord::Rollback,ActiveRecord::RecordNotDestroyed => e
      Rails.logger.error "Record is not destroyed #{e.message}"
      status[:success] = false
      status[:msg] = e.message
    end
    return status
  end


  def undo_transaction
    status = {success: true}
    transaction_status = @transaction.status_changed? ? @transaction.status_was : @transaction.status
    quantity = quantity_changed?() ? get_previous_quantity() : get_current_quantity()
    book = @old_book.nil? ? @book : @old_book
    begin
      Transaction.transaction do
        if transaction_status == "staged"
          book.update_available_count(quantity,false)
        else
          book.update_total_count(quantity,false)
        end
      end
    rescue  ActiveRecord::Rollback, ActiveRecord::RecordInvalid => e
      Rails.logger.error "Record is invalid #{e.message}"
      status[:success] = false
      status[:msg] = e.message
    end
    status[:transaction] = @transaction
    return status
  end

  def update_transaction
    # check important fields that have changed and do the necessary changes
    # fields that matter => quantity, book_id, status
    status = undo_transaction()
    if !status[:success]
      return status
    end
    create_transaction()
  end

private
  def get_current_quantity
    @transaction.initial_count - @transaction.end_count
  end

  def get_previous_quantity
    @transaction.initial_count_was - @transaction.end_count_was
  end

  def quantity_changed?
    @transaction.initial_count_changed? || @transaction.end_count_changed?
  end
end