class AddDefaultValueToBooks < ActiveRecord::Migration[5.0]
  def change
    change_column_default :books, :available_count, from: nil, to: 0
    change_column_default :books, :sold_count, from: nil, to: 0
    change_column_default :transactions, :quantity, from: nil, to: 0
    change_column_default :transactions, :suggested_count, from: nil, to: 0
    change_column_default :transactions, :initial_count, from: nil, to: 0
    change_column_default :transactions, :end_count, from: nil, to: 0
  end
end
