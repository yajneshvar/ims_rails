class Admin::ApplicationController < ApplicationController
  before_action :authorize_admin!

  def index
    @all_user = User.all
  end

  private

  def authorize_admin!
    authenticate_user!
      unless current_user.admin?
        redirect_to root_path, alert: "You must be an admin"
      end
  end
end
