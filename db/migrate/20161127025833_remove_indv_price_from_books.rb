class RemoveIndvPriceFromBooks < ActiveRecord::Migration[5.0]
  def change
    remove_column :books, :indv_price, :float
  end
end
