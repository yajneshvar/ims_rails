require 'rails_helper'

RSpec.describe "sub_channels/show", type: :view do
  before(:each) do
    @sub_channel = assign(:sub_channel, SubChannel.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
