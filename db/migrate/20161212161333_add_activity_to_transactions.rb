class AddActivityToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :transactions, :activity, :integer, default: 0
    add_column :transactions, :suggested_count, :integer
    add_column :transactions, :initial_count, :integer
    add_column :transactions, :end_count, :integer
  end
end
