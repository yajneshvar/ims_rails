class IssueTransaction

    def initialize(trans,book)
      @transaction = trans
      @book = book
      @old_book = @transaction.book_id_changed? && !@transaction.book_id_was.nil? ? Book.find(@transaction.book_id_was) : nil
    end

    def create_transaction
      #creating a new sale
      status = {success: true}
      status[:transaction] = @transaction
      @transaction.status = "confirmed"
      begin
        @transaction.transaction do
          @book.update_available_count(@transaction.quantity)
          @transaction.save!
        end
      rescue ActiveRecord::Rollback,ActiveRecord::RecordInvalid => e
        Rails.logger.error "Record is invalid #{e.message}"
        status[:success] = false
        status[:msg] = @transaction.errors.messages
      end

      return status
    end

    def delete_transaction
      status = {success: true, transaction: @transaction}
      begin
        Transaction.transaction do
          @transaction.lock!
          @book.update_available_count(@transaction.quantity,false)
          @transaction.destroy!
        end
      rescue  ActiveRecord::Rollback,ActiveRecord::RecordNotDestroyed => e
        Rails.logger.error "Record is not destroyed #{e.message}"
        status[:success] = false
        status[:msg] = e.message
      end
      return status
    end


    def undo_transaction
      status = {success: true}
      transaction_status = @transaction.status_changed? ? @transaction.status_was : @transaction.status
      quantity = @transaction.quantity_changed? ? @transaction.quantity_was : @transaction.quantity
      book = @old_book.nil? ? @book : @old_book
      begin
        Transaction.transaction do
            book.update_available_count(quantity,false)
        end
      rescue  ActiveRecord::Rollback, ActiveRecord::RecordInvalid => e
        Rails.logger.error "Record is invalid #{e.message}"
        status[:success] = false
        status[:msg] = e.message
      end
      status[:transaction] = @transaction
      return status
    end

    def update_transaction
      # check important fields that have changed and do the necessary changes
      # fields that matter => quantity, book_id, status
      status = undo_transaction()
      if !status[:success]
        return status
      end
      create_transaction()
    end

end