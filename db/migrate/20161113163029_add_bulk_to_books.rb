class AddBulkToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :bulk_price, :float
  end
end
