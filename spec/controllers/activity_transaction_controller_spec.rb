require 'rails_helper'

RSpec.describe TransactionsController, type: :controller do
  let(:user) {FactoryGirl.create(:user)}
  let(:transaction) {FactoryGirl.create(:transaction,:restock)}
  let(:issue_transaction) {FactoryGirl.create(:transaction,:issue)}
  let(:book){FactoryGirl.create(:book)}

  before do
    allow(controller).to receive(:authenticate_user!)
    allow(controller).to receive(:current_user).and_return(user)
  end

  context "restock transaction" do

    it "add to book stock" do
      @book = book
      @initial_available_count = @book.available_count
      @initial_sold_count = @book.sold_count
      quantity = 5
      post :create, params: {transaction: {book_id:@book.id, quantity: quantity, origin_location: "BookRoom1", activity: "restock"}}
      @book.reload
      expect(response).to redirect_to(transactions_path)
      expect(flash[:notice]).to eq "Transaction was successfully executed."
      expect(@book.available_count).to be @initial_available_count + quantity
      expect(@book.sold_count).to be @initial_sold_count
    end

  end

  context "Issue transaction" do
    it "reduces only available count" do
      @book = book
      @initial_available_count = @book.available_count
      @initial_sold_count = @book.sold_count
      quantity = 5
      post :create, params: {transaction: {book_id:@book.id, quantity: quantity, origin_location: "BookRoom1", activity: "issue"}}
      @book.reload
      expect(response).to redirect_to(transactions_path)
      expect(flash[:notice]).to eq "Transaction was successfully executed."
      expect(@book.available_count).to be @initial_available_count - quantity
      expect(@book.sold_count).to be @initial_sold_count
    end
  end


  context "Score transaction" do
    it "reduces only sold count" do
      @book = book
      @initial_available_count = @book.available_count
      @initial_sold_count = @book.sold_count
      quantity = 5
      post :create, params: {transaction: {book_id:@book.id, quantity: quantity, origin_location: "BookRoom1", activity: "score"}}
      @book.reload
      expect(response).to redirect_to(transactions_path)
      expect(flash[:notice]).to eq "Transaction was successfully executed."
      expect(@book.available_count).to be @initial_available_count
      expect(@book.sold_count).to be @initial_sold_count + quantity
    end
  end

  # context "Handle delete of a sale transactions" do
  #   before(:each) do
  #     @transaction = sale_transaction
  #     @book = @transaction.book
  #     @quantity = @transaction.quantity
  #     @initial_available_count = @book.available_count
  #     @initial_sold_count = @book.sold_count
  #   end
  #   it "deletes a sale transaction" do
  #     post :destroy, params: {id: @transaction.id}
  #     #expect(response).to redirect_to "/"
  #     @book.reload
  #     expect(flash[:notice]).to eq "Transaction was successfully destroyed"
  #     expect(@book.available_count).to be @initial_available_count + @quantity
  #     expect(@book.sold_count).to be @initial_sold_count - @quantity
  #   end
  # end
  #
  # context "Handle update of a sales transaction" do
  #   before(:each) do
  #     @transaction = sale_transaction
  #     @book = @transaction.book
  #     @initial_quantity = @transaction.quantity
  #     @updated_by =  2
  #     @initial_available_count = @book.available_count
  #     @initial_sold_count = @book.sold_count
  #   end
  #
  #   it "updates the increased quantity of the sales transaction" do
  #     post :update, params: {id: @transaction.id, transaction: {quantity: @initial_quantity + @updated_by } }
  #     @book.reload
  #     expect(flash[:notice]).to eq "Transaction was successfully updated"
  #     expect(@book.available_count).to be @initial_available_count - @updated_by
  #     expect(@book.sold_count).to be @initial_sold_count + @updated_by
  #   end
  #
  #   it "decreases the count by the reduced quantity" do
  #     post :update, params: {id: @transaction.id, transaction: {quantity: @initial_quantity - @updated_by } }
  #     @book.reload
  #     expect(flash[:notice]).to eq "Transaction was successfully updated"
  #     expect(@book.available_count).to be @initial_available_count + @updated_by
  #     expect(@book.sold_count).to be @initial_sold_count - @updated_by
  #   end
  # end

end
