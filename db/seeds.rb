# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'
unless User.exists?(email: "yajneshvar.arumugam@gmail.com")
User.create!(email: "yajneshvar.arumugam@gmail.com", password: "vyasadev@123", admin: true
) end


CSV::Converters[:blank_to_nil] = lambda do |field|
  field && field.empty? ? nil : field
end

#upload pricing information
csv = CSV.read("db/Pricing2016Upload.csv",{headers: true, header_converters: :symbol, skip_blanks: true, return_headers: false, converters: [:all,:blank_to_nil]})
csv.each do |row|
  unless Book.exists?(name: row.fetch(:name))
    puts "Parsing book #{row.fetch(:name)}"
    if(row.fetch(:name) != nil)
      Book.create(row.to_hash)
    else
      puts "Found an empty row"
    end
  end
end

#Add filters
bucket_filter = %w[Street In-temple Special\ Events Summer\ Festivals Sastradana Sets BTG\ Subs ]
channel_filter = %w[Street\ Festivals Bhakti\ Lounge Ind.\ Distributors In-temple Boutique Other Smart\ Tables Smart\ Boxes
Yoga\ Shows Rathayatra GTA\ Festivals Out-of-GTA\ Festivals SD\ Raised-external SD\ Raised-internal D2D SD\ Placements ]
sub_channel_filter = %w[Christmas\ Sweets Contact\ Table Diwali\ Sweets Eve\ of\ Bhakti Gifts Hallway MSF Outside\ Sun\ Feast
Sunday\ Feast Govindas  Sankirtan\ Sundays Website BakeSale  Tuesday\ Sanga SD\ Placement\ Team Yoga\ Shows ]
region_list = %w[TST\ Central TST\ West TST\ NWest TST\ East TST\ NEast]

region_list.each do |region|
  unless Region.exists?(name:region)
    Region.create(name: region)
  end
end

bucket_filter.each do |filter|
  unless Bucket.exists?(name: filter)
    Bucket.create(name: filter)
  end
end

channel_filter.each do |filter|
  unless Channel.exists?(name: filter)
    Channel.create(name: filter)
  end
end

sub_channel_filter.each do |filter|
  unless SubChannel.exists?(name: filter)
    SubChannel.create(name: filter)
  end
end