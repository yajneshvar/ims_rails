class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :name, null: false, default: ""
      t.string :code, index: true
      t.integer :temple_price
      t.integer :indv_price
      t.integer :reg_price
      t.string :language, null: false, default: ""
      t.integer :quantity

      t.timestamps
    end
  end
end
