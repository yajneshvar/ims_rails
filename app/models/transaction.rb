class Transaction < ApplicationRecord
  belongs_to :book, inverse_of: :transactions
  belongs_to :event, inverse_of: :transactions, optional: true
  enum activity: {sale: 0,consignment: 1,restock: 2, issue: 3, score: 4}
  enum status: {confirmed: 0, staged: 0}
  #validation
  validates :issued_by,:book_id,:activity,:status, presence: true
  validate :initial_count_greater_than_zero,:initial_count_less_than_end_count, if: :consigment?

  def move?
    activity == "move"
  end

  def consigment?
    activity == "consignment"
  end

  def initial_count_greater_than_zero
    if initial_count.nil? || initial_count < 0
      errors.add(:initial_count,"Initial count is less than zero or nil")
    end
  end

  def initial_count_less_than_end_count
    if initial_count < end_count
      errors.add(:end_count, "End count is more than intial count")
    end
  end

  def set_pending_amount
    pending_amount = sale_price - pending_amount
  end

end
