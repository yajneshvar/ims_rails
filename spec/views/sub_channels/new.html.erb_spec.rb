require 'rails_helper'

RSpec.describe "sub_channels/new", type: :view do
  before(:each) do
    assign(:sub_channel, SubChannel.new(
      :name => "MyString"
    ))
  end

  it "renders new sub_channel form" do
    render

    assert_select "form[action=?][method=?]", sub_channels_path, "post" do

      assert_select "input#sub_channel_name[name=?]", "sub_channel[name]"
    end
  end
end
