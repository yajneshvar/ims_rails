class AddRegionToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :transactions, :region, :integer, default: 0
    add_column :transactions, :comments, :string
  end
end
