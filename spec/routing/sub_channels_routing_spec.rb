require "rails_helper"

RSpec.describe SubChannelsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sub_channels").to route_to("sub_channels#index")
    end

    it "routes to #new" do
      expect(:get => "/sub_channels/new").to route_to("sub_channels#new")
    end

    it "routes to #show" do
      expect(:get => "/sub_channels/1").to route_to("sub_channels#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sub_channels/1/edit").to route_to("sub_channels#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sub_channels").to route_to("sub_channels#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/sub_channels/1").to route_to("sub_channels#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/sub_channels/1").to route_to("sub_channels#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sub_channels/1").to route_to("sub_channels#destroy", :id => "1")
    end

  end
end
