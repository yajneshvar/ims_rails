class AddBucketToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :transactions, :bucket, :string
    add_column :transactions, :channel, :string
    add_column :transactions, :sub_channel, :string
  end
end
