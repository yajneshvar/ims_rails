FactoryGirl.define do
  sequence :event_name do |n|
    "Event#{n}"
  end

  sequence :event_location do |n|
    "Location#{n}"
  end

  factory :event do
    name {generate :event_name}
    date Date.today.to_s
    location {generate :event_location}
    coordinator "MyString"

    trait :completed do
    completed true
    end

    factory :event_with_transactions do

    transient do
      transactions_count 5
    end

    after(:create) do |event,evaluator|
      create_list(:transaction,evaluator.transactions_count, :consignment, :confirmed, event: event)
    end

    end

  end
end
