class AddCompletedToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :completed, :boolean, default: false
  end
end
