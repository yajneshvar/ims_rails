# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161231144231) do

  create_table "books", force: :cascade do |t|
    t.string   "name",              default: "", null: false
    t.string   "code"
    t.float    "temple_price"
    t.float    "reg_price"
    t.string   "language",          default: "", null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.float    "bulk_price"
    t.float    "distributor_price"
    t.integer  "volumes"
    t.string   "category"
    t.integer  "available_count",   default: 0
    t.integer  "sold_count",        default: 0
    t.index ["code"], name: "index_books_on_code"
  end

  create_table "buckets", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "channels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "date"
    t.string   "location"
    t.string   "coordinator"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "completed",   default: false
  end

  create_table "inventories", force: :cascade do |t|
    t.integer "book_id"
    t.integer "room_id"
    t.integer "available_count"
    t.index ["book_id", "room_id"], name: "index_inventories_on_book_id_and_room_id", unique: true
    t.index ["book_id"], name: "index_inventories_on_book_id"
    t.index ["room_id"], name: "index_inventories_on_room_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["name"], name: "index_rooms_on_name", unique: true
  end

  create_table "sub_channels", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "quantity",        default: 0
    t.string   "issued_by"
    t.float    "sale_price",      default: 0.0
    t.float    "amount_paid",     default: 0.0
    t.float    "pending_amount",  default: 0.0
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "book_id"
    t.integer  "activity",        default: 0
    t.integer  "suggested_count", default: 0
    t.integer  "initial_count",   default: 0
    t.integer  "end_count",       default: 0
    t.integer  "status",          default: 0
    t.string   "origin_location"
    t.string   "final_location"
    t.integer  "event_id"
    t.string   "bucket"
    t.string   "channel"
    t.string   "sub_channel"
    t.integer  "region",          default: 0
    t.string   "comments"
    t.index ["book_id"], name: "index_transactions_on_book_id"
    t.index ["event_id"], name: "index_transactions_on_event_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
