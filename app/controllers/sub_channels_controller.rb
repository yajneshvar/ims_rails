class SubChannelsController < ApplicationController
  before_action :set_sub_channel, only: [:show, :edit, :update, :destroy]

  # GET /sub_channels
  # GET /sub_channels.json
  def index
    @sub_channels = SubChannel.all
    render json: @sub_channels
  end

  # GET /sub_channels/1
  # GET /sub_channels/1.json
  def show
    render json: @sub_channel
  end

  # GET /sub_channels/new
  def new
    @sub_channel = SubChannel.new
  end

  # GET /sub_channels/1/edit
  def edit
  end

  # POST /sub_channels
  # POST /sub_channels.json
  def create
    @sub_channel = SubChannel.new(sub_channel_params)
      if @sub_channel.save
        render json: @sub_channel, status: :created, location: @sub_channel
      else
        render json: @sub_channel.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /sub_channels/1
  # PATCH/PUT /sub_channels/1.json
  def update
      if @sub_channel.update(sub_channel_params)
        render json: @sub_channel, status: :ok, location: @sub_channel
      else
        render json: @sub_channel.errors, status: :unprocessable_entity
      end
  end

  # DELETE /sub_channels/1
  # DELETE /sub_channels/1.json
  def destroy
    @sub_channel.destroy
      render json: @sub_channel
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sub_channel
      @sub_channel = SubChannel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sub_channel_params
      params.permit(:name)
    end
end
