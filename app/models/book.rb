class Book < ApplicationRecord
  include Filterable
  has_many :transactions, inverse_of: :book
  scope :likeName, -> (name) { where("name like ?", "#{name}%")}
  scope :language, -> (language) {where language: language}


  def add_sold_count(quantity)
    self.lock!
    if available_count == 0
      Rails.logger.error "Available Count is nill"
    end
    self.sold_count += quantity
    self.save!
  end

  def sub_sold_count(quantity)
    self.lock!
    if available_count == 0
      Rails.logger.error "Available Count is nill"
    end
    if sold_count == 0
      Rails.logger.error "Trying to undo sale for a book that has not been sold #{@book} "
    end
    self.sold_count -= quantity
    self.save!
  end

  def update_available_count(quantity,reduce=true)
    self.lock!
    if(reduce)
      self.available_count -= quantity
    else
      self.available_count += quantity
    end
    self.save!
  end

  def update_total_count(quantity,reduce=true)
    self.lock!
    if(reduce)
      self.available_count -= quantity
      self.sold_count += quantity
    else
      self.available_count += quantity
      self.sold_count -= quantity
    end
    self.save!
  end

end
