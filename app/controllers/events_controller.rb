class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    render json: @events
  end

  # GET /events/1
  # GET /events/1.json
  def show
    render json: @event
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
      if @event.save
         render json: @event, status: :created, location: @event
      else
        render json: @event.errors, status: :unprocessable_entity
      end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
      if @event.update(event_params)
        render json: @event, status: :ok, location: @event
      else
       render json: @event.errors, status: :unprocessable_entity
      end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    render json: { }, status: :ok

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
      @transactions = @event.transactions
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.permit(:name, :date, :location, :coordinator)
    end

end
