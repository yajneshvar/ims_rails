class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.belongs_to :books, index: true
      t.integer :quantity
      t.string :issued_by
      t.float :sale_price
      t.float :amount_paid
      t.float :pending_amount

      t.timestamps
    end
  end
end
