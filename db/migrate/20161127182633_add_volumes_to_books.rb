class AddVolumesToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :volumes, :integer
  end
end
