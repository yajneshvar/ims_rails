class AddLocationToTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :transactions, :origin_location, :string
    add_column :transactions, :final_location, :string
  end
end
