class ChangePriceTypeInBooks < ActiveRecord::Migration[5.0]
  def change
        change_column :books, :temple_price, :float
        change_column :books, :indv_price, :float
        change_column :books, :reg_price, :float
  end
end
