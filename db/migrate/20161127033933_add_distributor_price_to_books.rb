class AddDistributorPriceToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :distributor_price, :float
  end
end
