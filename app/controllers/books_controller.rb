require 'csv'
class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  # GET /books
  # GET /books.json
  def index
    @books = Book.all
    render json: @books
  end

  # GET /books/1
  # GET /books/1.json
  def show
    render json: @book
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end


  # POST /books
  # POST /books.json
  def create
    @book = Book.new(book_params)

      if @book.save
         render :show, status: :created, location: @book
      else
        render json: @book.errors, status: :unprocessable_entity
      end
  end

#  def upload
#    @file = books_param
#    csv = CSV.new(@file.to_io,{headers: true, header_converters: :symbol})
#    ProcessPricesJob.perform_later(csv)
#    print "Name of file #{@file.original_filename}"
#  end

  # PATCH/PUT /books/1
  # PATCH/PUT /books/1.json
  def update
      if @book.update(book_params)
        render json: @book
      else
        render json: @book.errors, status: :unprocessable_entity
      end
  end

  # DELETE /books/1
  # DELETE /books/1.json
  def destroy
    @book.destroy
    render json: @book, status: :ok
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.permit(:name, :code, :temple_price, :distributor_price, :reg_price, :language, :quantity,:available_count)
    end

end
