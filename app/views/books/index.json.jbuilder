json.array!(@books) do |book|
  json.extract! book, :id, :name, :code, :temple_price, :distributor_price, :reg_price, :language, :volumes
  json.url book_url(book, format: :json)
end
