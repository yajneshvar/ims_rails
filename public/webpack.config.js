var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname);
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
    entry: APP_DIR + '/index.jsx',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module : {
        loaders : [
            {
                test : /\.(jsx|js)?/,
                include : APP_DIR,
                use:[
                    "babel-loader"
                ]

            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use:[
                    "style-loader","css-loader"
                ]
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use:[
                    'url-loader?limit=8192'
                ]
            }

        ]
    },
    devtool: 'source-map',
    watch: true
};

module.exports = config;