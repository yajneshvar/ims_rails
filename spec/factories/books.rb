FactoryGirl.define do
  sequence :name do |n|
    "Book#{n}"
  end

  sequence :code do |n|
    "Code#{n}"
  end

  factory :book do
    name {generate :name}
    code {generate :code}
    temple_price 1
    distributor_price 1
    reg_price 1
    language "English"
    available_count 10
    sold_count 1
  end
end
