class TransactionService

def initialize(transaction_params,old_transaction)
  Rails.logger.info("Transaction params is #{transaction_params}")
  #Extract the book from the params
  @transaction = old_transaction.nil? ? Transaction.new(transaction_params) : old_transaction
  @transaction.assign_attributes(transaction_params) unless old_transaction.nil? || transaction_params.nil?
  @book = @transaction.book
  setup_service_hash()
  @service_object = @services[@transaction.activity.to_sym].new(@transaction,@book)
  @old_service_object = old_transaction.nil? ? nil : @services[old_transaction.activity.to_sym].new(old_transaction,old_transaction.book)
end

def setup_service_hash
  @services = {sale: SaleTransaction, consignment: ConsignmentTransaction, restock: RestockTransaction, issue: IssueTransaction, score: ScoreTransaction}
end

def create_transaction
  status = @service_object.create_transaction()
  return status
end

def delete_transaction
  @service_object.delete_transaction()
end

def update_transaction
  #TODO handle scenario of changing activity
  status = nil
  if @transaction.activity_changed?
    status = @old_service_object.undo_transaction()
    @service_object.create_transaction() if status[:success]
  else
    status = @service_object.update_transaction()
  end
  status
end

end
