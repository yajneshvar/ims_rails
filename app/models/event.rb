class Event < ApplicationRecord
  has_many :transactions, inverse_of: :event, dependent: :delete_all
end
