json.array!(@events) do |event|
  json.extract! event, :id, :name, :date, :location, :coordinator
  json.url event_url(event, format: :json)
end
