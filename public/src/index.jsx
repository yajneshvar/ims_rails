import React from 'react';
import {render} from 'react-dom';
import Boostrap from './bower_components/bootstrap/dist/css/bootstrap.css';
import App from "./App.jsx";
import LogIn from "./components/LogIn.jsx";

render(<LogIn/>, document.getElementById('app'));
