Rails.application.routes.draw do

  resources :sub_channels
  resources :channels
  resources :buckets
  resources :events do
      resources :transactions
  end

  resources :books
  resources :transactions

  namespace :admin do
    root 'application#index'
  end
  root "static_pages#home"
  get "static_pages/help"

  post 'auth_user', to: 'authentication#authenticate_user'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
